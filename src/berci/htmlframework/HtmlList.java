package berci.htmlframework;

/**
 * This class represents an HTML list view element. It is either an ul or ol type.
 */
public class HtmlList extends HtmlContainer {

    /**
     * This class is to throw an exception if an invalid object would be added to an HtmlList object.
     */
    public static class HtmlElementNotSupported extends RuntimeException
    {
        public HtmlElementNotSupported() {
            super();
        }

        public HtmlElementNotSupported(String message)
        {
            super(message);
        }

        public HtmlElementNotSupported(String message, Throwable cause)
        {
            super(message, cause);
        }

        public HtmlElementNotSupported(Throwable cause)
        {
            super(cause);
        }
    }


    /**
     * Inner class for the elements in the HTML List
     */
    public static class Li extends HtmlContainer
    {
        public Li(HtmlElement e) {
            super();
            addHtmlElement(e);
        }

        @Override
        public String getHtmlCode() {

            StringBuilder builder = new StringBuilder();
            builder.append("<li ");
            builder.append(getAllHtmlFormatingElements());
            builder.append(">\n");


            for(HtmlElement e : elements)
            {
                builder.append("\t");
                builder.append(e.getHtmlCode());
            }

            builder.append("\n");
            builder.append("</li>");
            return builder.toString();
        }
    }

    private boolean unordered;

    /**
     * @param unordered If it is true then the generated HTML list will be using ul list container, otherwise ol.
     */
    public HtmlList(boolean unordered) {
        super();
        this.unordered = unordered;
    }

    /**
     * Adds a Li element to the list.
     * @param element Only the Li object is accepted
     */
    public void addHtmlElement(Li element) {
        super.addHtmlElement(element);
    }

    /**
     * This class can only add Li objects to its container.
     * @see Li
     * @param element Can only be Li object. If not, HtmlElementNotSupported exception is thrown.
     */
    @Deprecated
    public void addHtmlElement(HtmlElement element) {
        if(!(element instanceof Li))
        {
            throw new HtmlElementNotSupported("Only Li object can be added to HtmlList class");
        }
        addHtmlElement((Li)element);
    }

    @Override
    public String getHtmlCode() {
        StringBuilder builder = new StringBuilder();
        builder.append(unordered ? "<ul " : "<ol");
        builder.append(getAllHtmlFormatingElements());
        builder.append(">\n");


        for(HtmlElement e : elements)
        {
            builder.append("\t");
            builder.append(e.getHtmlCode());
            builder.append("\n");
        }

        builder.append(unordered ? "</ul>" : "</ol>");

        return builder.toString();
    }
}
