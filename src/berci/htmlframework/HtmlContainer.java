package berci.htmlframework;

import java.util.ArrayList;

/**
 * Abstract base class for Html container elements such as table, select, dropdown, etc.
 */
public abstract class HtmlContainer extends HtmlElement {

    protected ArrayList<HtmlElement> elements;

    public HtmlContainer() {
         elements = new ArrayList<>();
    }

    /**
     * Adds a Html element to the container.
     * @param element Can be a HtmlPrimitive or HtmlContainer as well.
     */
    public void addHtmlElement(HtmlElement element)
    {
        elements.add(element);
    }

    /**
     * Getter for the elements.
     * @return Returns the containing HTML elements
     */
    public ArrayList<HtmlElement> getElements()
    {
        return elements;
    }
}
