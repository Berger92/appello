package berci.htmlframework;

import java.util.*;

/**
 * Abstract base class for the HTML elements.
 * Derived classes must implement the getHtmlCode function.
 */
public abstract class HtmlElement {

    protected Map<String, String> htmlAttributes;
    protected Map<String, String> styleElements;
    protected ArrayList<String> classes;




    public HtmlElement()
    {
        htmlAttributes = new LinkedHashMap<>();
        styleElements = new LinkedHashMap<>();
        classes = new ArrayList<>();
    }

    /**
     * With this method you are able to add HTML attributes to the view element, i.e id="button1".
     * Refreshes the value if same attribute added more times.
     *
     * @param attribute The name of the HTML Attribute, i.e id
     * @param value The value for the HTML attribute, i.e "button1"
     *
     */
    public void addHtmlAttribute(String attribute, String value)
    {
        htmlAttributes.put(attribute, value);
    }

    /**
     * Using this method you are able to add style elements so that you are able to customize the appearance of the view element.
     * Refreshes the value if same attribute added more times.
     *
     * @param attribute The name of the Style attribute, i.e border
     * @param value The value for the style attribut, i.e "solid 1px black"
     */
    public void addStyleElement(String attribute, String value)
    {
        styleElements.put(attribute, value);
    }

    /**
     * This methods add a CSS class to the element.
     *
     * @param cssClass The name of the CSS class.
     */
    public void addCssClass(String cssClass)
    {
        classes.add(cssClass);
    }

    /**
     * Generates HTML code of the htmlAttributes object.
     *
     * @return Returns the generated HTML code for the HTML attributes
     */
    protected String getHtmlCodeOfAttributes()
    {
        StringBuilder builder = new StringBuilder();

        if(htmlAttributes.size() > 0)
        {
            Iterator<String> it = htmlAttributes.keySet().iterator();
            while(it.hasNext())
            {
                String attribute = it.next();
                String value = htmlAttributes.get(attribute);

                builder.append(" ");
                builder.append(attribute);
                builder.append("=");
                builder.append("\"");
                builder.append(value);
                builder.append("\"");
            }
        }

        return builder.toString();
    }

    /**
     * Generates CSS code of the classes object.
     *
     * @return Returns the generated CSS code
     */
    protected String getCSSClasses()
    {
        StringBuilder builder = new StringBuilder();

        if(classes.size() > 0)
        {
            builder.append("class=\"") ;
            builder.append(classes.get(0));

            for(int i = 1; i < classes.size(); i++)
            {
                builder.append(" ");
                builder.append(classes.get(i));
            }

            builder.append("\"");
        }

        return builder.toString();
    }

    /**
     * Generates style HTML attributes.
     *
     * @return Returns the generated style HTML attribute
     */
    protected String getStyleElements()
    {
        String style = "";
        StringBuilder builder = new StringBuilder();

        if(styleElements.size() > 0)
        {
            builder.append("style=\"");

            Iterator<String> it = styleElements.keySet().iterator();
            while(it.hasNext())
            {
                String attribute = it.next();
                String value = styleElements.get(attribute);

                builder.append(attribute);
                builder.append(":");
                builder.append(value);

                if (it.hasNext())
                    builder.append("; ");
                else
                    builder.append(";");
            }
            builder.append("\"");
        }

        return builder.toString();
    }

    /**
     * @return Returns all of the formatting elements in order of CSS classes, HTML attributes, style elements.
     */
    protected String getAllHtmlFormatingElements()
    {
        StringBuilder builder = new StringBuilder();

        //add css classes
        builder.append(getCSSClasses());
        if(htmlAttributes.size() > 0)
            builder.append(" ");

        //add html attributes
        builder.append(getHtmlCodeOfAttributes());

        if(styleElements.size() > 0)
            builder.append(" ");

        //add html style attributes
        builder.append(getStyleElements());

        return builder.toString();
    }

    /**
     * This abstract method that generates HTML code using its objects.
     * All child classes must implement this function.
     * @return Returns the HTML code.
     */
    public abstract String getHtmlCode();

}
