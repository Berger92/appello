package berci.htmlframework;

import java.util.ArrayList;

/**
 * <h1>Class for the select HTML view element.</h1>
 * <p>This class represents a HTML select view element</p>
 */

public class Select extends HtmlContainer{

    private class Option extends HtmlPrimitive
    {
        private String text;
        private String value;

        /**
         * @param text The text that the users can see in the dropdown list
         * @param value The value that is passed in the POST request
         */
        public Option(String text, String value) {
            super();
            this.text = text;
            this.value = value;
        }

        @Override
        public String getHtmlCode() {

            StringBuilder builder = new StringBuilder();

            builder.append("<option ");
            builder.append("value=\"");
            builder.append(value);
            builder.append("\">");
            builder.append(text);
            builder.append("</option>");

            return builder.toString();
        }
    }

    private ArrayList<Option> options;

    public Select() {
        super();
        options = new ArrayList<>();
    }

    /**
     * Adds an option to the list of selection.
     * @param text The text that the users can see in the dropdown list
     * @param value The value that is passed in the POST request
     */
    void addOption(String text, String value)
    {
        options.add(new Option(text, value));
    }

    @Override
    public String getHtmlCode() {

        StringBuilder builder = new StringBuilder();

        builder.append("<select>\n\t");

        int i = 0;
        for(Option e : options)
        {
            builder.append(e.getHtmlCode());
            if(i != options.size() - 1)
            {
                builder.append("\n\t");
            } else {
                builder.append("\n");
            }
            i++;
        }

        builder.append("</select>");

        return builder.toString();
    }
}
