package berci.htmlframework;

/**
 * Div html view element is acts like either HtmlPrimitive or a HtmlContainer object.
 * It can contain either a simple string or a html view element.
 * Use the overloaded addInnerContent method to set its content.
 * @see HtmlPrimitive
 * @see HtmlContainer
 */
public class Div extends HtmlContainer {

    public Div() {
        super();
    }

    /**
     * Constructor that takes a string and adds it to the the div's content.
     * @param content String to be displayed in te div element
     */
    public Div(String content) {
        super();
        addHtmlElement(new HtmlBasic.HtmlString(content));
    }

    /**
     * Constructor that takes an berci.htmlframework.HtmlElement and adds it to the the div's content.
     * @param element A HTML element, either a baerci.htmlframework.HtmlPrimtive or berci.htmlframework.HtmlContainer to be displayed in the div element.
     */
    public Div(HtmlElement element)
    {
        super();
        addHtmlElement(element);
    }

    /**
     * This method adds to the div element's contents a mere string.
     * @param content The string you want to add to the div element.
     */
    public void addInnerContent(String content)
    {
        addHtmlElement(new HtmlBasic.HtmlString(content));
    }




    @Override
    public String getHtmlCode() {


        StringBuilder builder = new StringBuilder();
        builder.append("<div ");
        builder.append(getAllHtmlFormatingElements());
        builder.append(">");

        for(HtmlElement e : elements)
        {
            builder.append("\n\t");
            builder.append(e.getHtmlCode());
        }

        builder.append("\n");
        builder.append("</div>");

        return builder.toString();
    }
}
