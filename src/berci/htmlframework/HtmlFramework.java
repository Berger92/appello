package berci.htmlframework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * <h1>The engine of the program</h1>
 * <p>This class manages the HTML sites. It can create web pages, add JS or CSS files to it.</p>
 */
public class HtmlFramework {

    private ArrayList<HtmlElement> elements;
    private ArrayList<String> cssFiles;
    private ArrayList<String> jsFiles;

    private static HtmlFramework ourInstance = new HtmlFramework();

    public static HtmlFramework getInstance()
    {
        return ourInstance;
    }

    private HtmlFramework()
    {
        elements = new ArrayList<>();
        cssFiles = new ArrayList<>();
        jsFiles = new ArrayList<>();
    }

    /**
     * Use this method to add view elements to the web page.
     * @param newElement An object that derives from berci.htmlframework.HtmlElement class.
     */
    public void addHtmlElement(HtmlElement newElement)
    {
        elements.add(newElement);
    }

    /**
     * Generates the HTML code form the object's attributes.
     * @param element An object that derives from berci.htmlframework.HtmlElement class
     * @return Returns the element's HTML code as String.
     */
    public String display(HtmlElement element)
    {
        return element.getHtmlCode();
    }

    /**
     * Adds a CSS file to the HTML file.
     * @param fileName Always include path and extension!
     */
    public void addCssFile(String fileName)
    {
        cssFiles.add(fileName);
    }

    /**
     * Adds a JS file to the HTML file.
     * @param fileName Always include path and extension!
     */
    public void addJsFile(String fileName)
    {
        jsFiles.add(fileName);
    }

    /**
     * @param title Sets the web page's title.
     * @return Returns the generated head part of the HTML dom.
     */
    private StringBuilder generateHead(String title)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<html>\n");
        builder.append("<head>\n");
        builder.append("\t<meta charset=\"UTF-8\">\n");
        builder.append("\t<title>");
        builder.append(title);
        builder.append("</title>\n\n");

        builder.append("<!--CSS Files-->\n");
        for(String i : cssFiles)
        {
            builder.append("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"");
            builder.append(i);
            builder.append("\">\n");
        }

        builder.append("\n");
        builder.append("</head>");

        return builder;
    }

    private void generateDomBasicPart(StringBuilder fraction, String pageTitle)
    {
        fraction.append("<!DOCTYPE html>\n\n");

        //generate Head
        fraction.append(generateHead(pageTitle));
        fraction.append("\n");

    }

    private void generateBody(StringBuilder fraction)
    {
        for(HtmlElement e : elements)
        {
            fraction.append(e.getHtmlCode());
            fraction.append("\n");
        }
    }

    private void generateJSFiles(StringBuilder fraction)
    {
        fraction.append("\n");
        fraction.append("<!--JS Files-->\n");

        for(String i : jsFiles)
        {
            fraction.append("<script src=\"");
            fraction.append(i);
            fraction.append("\"></script>\n");
        }
        fraction.append("\n");
    }

    /**
     * Generates the HTML web page from the added HtmlElements
     * @param fileName Sets the HTML file's name.
     * @param pageTitle Sets the web page's title.
     * @return Returns the generated HTML file.
     * @throws IOException If writing to the file is impossible an IOException is thrown.
     */
    public File generateHTML(String fileName, String pageTitle) throws IOException {


        StringBuilder builder = new StringBuilder();

        //Generate Header
        StringBuilder part1 = new StringBuilder();
        Thread t1 = new Thread( () -> {
            generateDomBasicPart(part1, pageTitle);
        });
        t1.start();


        //Generate Body
        StringBuilder part2 = new StringBuilder();
        Thread t2 = new Thread( () -> {
            generateBody(part2);
        });
        t2.start();


        //Generate JS
        StringBuilder part3 = new StringBuilder();
        Thread t3 = new Thread( () -> {
            generateJSFiles(part3);
        });
        t3.start();


        //generate dom
        try {
            t1.join();
            builder.append(part1);

            builder.append("<body>");
            t2.join();
            builder.append(part2);

            t3.join();
            builder.append(part3);
            builder.append("</body>\n");
            builder.append("</html>");

        } catch (InterruptedException e) {
            System.out.println("Something went wrong when synchronizing threads: ");
            e.printStackTrace();
            builder.setLength(0);
            builder.append(new Div("Something went wrong when synchronizing threads!").getHtmlCode());
        }


        //Write to File
        File dir = new File(this.getClass().getResource("/").getPath());
//        File dir = new File("html");
        dir.mkdirs();
        File file = new File(dir, fileName+ ".html");

        FileWriter writer = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write(builder.toString());
        bw.close();

        return file;

    }
}
