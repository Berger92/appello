package berci.htmlframework;

/**
 * This class stands for the image view HTML element.
 */
public class Img extends HtmlPrimitive {

    /**
     * Sets the src HTML attribute from the parameter.
     * @param src Sets the src HTML attribute.
     */
    public Img(String src) {
        innerHtmlValue = src;
    }

    @Override
    public String getHtmlCode() {

        StringBuilder builder = new StringBuilder();
        builder.append("<img src=\"");
        builder.append(innerHtmlValue);
        builder.append("\" ");
        builder.append(getAllHtmlFormatingElements());
        builder.append(">");

        return builder.toString();
    }
}
