package berci.htmlframework;

/**
 * This class represents a simple HTML view element such as button, input, p, h1, h2, etc.
 */
public abstract class HtmlPrimitive extends HtmlElement {

    protected String innerHtmlValue;


    /**
     * @param value Sets the HTML attribute
     */
    public void setHtmlValue(String value)
    {
        innerHtmlValue = value;
    }

    /**
     * @return Returns the HTML attribute
     */
    public String getInnerHtmlValue()
    {
        return innerHtmlValue;
    }

}
