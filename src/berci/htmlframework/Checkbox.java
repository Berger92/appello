package berci.htmlframework;

/**
 * This class represents a checkbox HTML element.
 */
public class Checkbox extends HtmlPrimitive {


    private HtmlBasic.Label label;
    private String id;

    /**
     * @param label The text that is visible next to the checkbox.
     * @param id The id for the checkbox to bind the label to it.
     */
    public Checkbox(String label, String id) {
        super();
        this.id = id;
        this.label = new HtmlBasic.Label(id, label);
    }

    @Override
    public String getHtmlCode() {

        StringBuilder builder = new StringBuilder();

        builder.append("<input type=\"checkbox\" id=\"");
        builder.append(id);
        builder.append("\"");
        builder.append(getAllHtmlFormatingElements());
        builder.append("/>\n");
        builder.append(label.getHtmlCode());

        return builder.toString();
    }

    /**
     * @return Returns the label object that is bind to the checkbox. Formatting elements now can be added to the label as well.
     */
    public HtmlBasic.Label getLabel()
    {
        return label;
    }
}
