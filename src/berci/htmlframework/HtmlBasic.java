package berci.htmlframework;

/**
 * This class contains the simplest html elements.
 * It should make your work faster. It generates string from basic html elements, such as b, p, span etc.
 * Such types are commonly used, so they can be used as strings as well as objects.
 * This class can also generate material-designed icons.
 */
public class HtmlBasic {

    /**
     * This class represents a simple paragraph.
     */
    public static class Paragraph extends HtmlContainer
    {

        public Paragraph(String text) {
            super();
            addHtmlElement(new HtmlString(text));
        }

        public Paragraph() {
            super();
        }

        @Override
        public String getHtmlCode() {
            StringBuilder builder = new StringBuilder();
            builder.append("<p ");
            builder.append(getAllHtmlFormatingElements());
            builder.append(">\n");

            for(HtmlElement e : elements)
            {
                builder.append("\t");
                builder.append(e.getHtmlCode());
                builder.append("\n");
            }

            builder.append("</p>");

            return builder.toString();
        }
    }

    /**
     * This class represents a span view element.
     */
    public static class Span extends HtmlContainer
    {

        public Span(String text) {
            super();
            addHtmlElement(new HtmlString(text));
        }

        public Span() {
            super();
        }

        @Override
        public String getHtmlCode() {

            StringBuilder builder = new StringBuilder();
            builder.append("<span ");
            builder.append(getAllHtmlFormatingElements());
            builder.append(">\n");

            for(HtmlElement e : elements)
            {
                builder.append("\t");
                builder.append(e.getHtmlCode());
                builder.append("\n");
            }

            builder.append("</span>");

            return builder.toString();
        }
    }

    /**
     * This class generates google-designed icons.
     */
    public static class MaterialIcon extends HtmlPrimitive
    {

        public MaterialIcon(String icon) {
            super();
            innerHtmlValue = icon;
            addCssClass("material-icons");
        }

        @Override
        public String getHtmlCode() {
            StringBuilder builder = new StringBuilder();

            builder.append("<i ");
            builder.append(getAllHtmlFormatingElements());
            builder.append(">");
            builder.append(innerHtmlValue);
            builder.append("</i>");

            return builder.toString();
        }
    }

    /**
     * Label class label HTML element
     */
    public static class Label extends HtmlPrimitive
    {
        private String id;
        private String text;

        public Label(String id, String text) {
            this.id = id;
            this.text = text;
        }

        @Override
        public String getHtmlCode() {

            StringBuilder builder = new StringBuilder();

            builder.append("<label for=\"");
            builder.append(id);
            builder.append("\" ");
            builder.append(getAllHtmlFormatingElements());
            builder.append(">");
            builder.append(text);
            builder.append("</label>");

            return builder.toString();
        }
    }

    /**
     * This is a helper class to store simple strings as HtmlElements.
     */
    public static class HtmlString extends HtmlPrimitive
    {
        public HtmlString(String value) {
            super();
            innerHtmlValue = value;
        }

        @Override
        public String getHtmlCode() {
            return innerHtmlValue;
        }
    }

    /**
     * @param text Text you want to make bold.
     * @return Returns the bold text's HTML code.
     */
    static String generateBoldText(String text)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<b>").append(text).append("</b>");
        return builder.toString();
    }

    /**
     * @param text Text you want to make italic.
     * @return Returns the italic text's HTML code.
     */
    static String generateItalicText(String text)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<i>").append(text).append("</i>");
        return builder.toString();
    }

    /**
     * @param text Text you want to make underlined.
     * @return Returns the underlined text's HTML code.
     */
    static String generateUnderlinedText(String text)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<u>").append(text).append("</u>");
        return builder.toString();
    }

    /**
     * @param text Text that is present in the paragraph.
     * @return Returns a simple paragraph's HTML code.
     */
    static String generateParagraph(String text)
    {
        return new Paragraph(text).getHtmlCode();
    }

    /**
     * @param text The code snippet.
     * @return Returns the code snippet surrounded with pre.
     */
    static String generatePre(String text)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("<pre>").append(text).append("</pre>");
        return builder.toString();
    }

    /**
     * @param text The text that is present in the span element.
     * @return Returns a simple span's HTML code.
     */
    static String generateSpan(String text)
    {
        return new Span(text).getHtmlCode();
    }
}
