package berci.htmlframework;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * <h1>JAVA-based HTML Framework</h1>
 * <p>The Main program implements an application that
 * creates an HtmlFramework object to display HTML elements
 * such as Buttons, Links and Containers.</p>
 *
 * @author  Bertalan Eck
 * @version 1.1
 * @since   2015-11-01
 */


public class Main {

    private HtmlFramework instance;

    /**
     * Adds the CSS files
     */
    private void addCSSFiles()
    {
        instance.addCssFile("https://fonts.googleapis.com/icon?family=Material+Icons");
        instance.addCssFile("css/materialize.min.css");
        instance.addCssFile("css/site.css");
    }

    /**
     * Adds the JavaScript files
     */
    private void addJSFiles()
    {
        instance.addJsFile("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js");
        instance.addJsFile("js/materialize.min.js");
        instance.addJsFile("js/site.js");
    }

    /**
     * Adds the Header
     */
    private void addHeader(Div header)
    {
        header.addCssClass("red lighten-1");
        header.addStyleElement("margin-top", "0.5rem;");

        Header header_h1 = new Header(1, "JAVA-based HTML Framework");
        header_h1.addCssClass("white-text center-align");
        header_h1.addStyleElement("margin-bottom", "0px");
        header_h1.addStyleElement("margin-top", "0px");

        header.addHtmlElement(header_h1);
    }

    /**
    * Adds the information about the author
    */
    private void addAboutAuthor(Div card)
    {
        card.addCssClass("card-panel blue lighten-1 z-depth-1");
        card.addStyleElement("margin-bottom", "0px");
        card.addStyleElement("margin-top", "0px");
        card.addStyleElement("border-top", "1px solid white");
        card.addStyleElement("border-bottom", "1px solid white");

        Div wrapper = new Div();
        wrapper.addCssClass("row valign-wrapper");

        Div img_container = new Div();
        img_container.addCssClass("col s2");

        Img profile = new Img("profile.jpg");
        profile.addCssClass("circle responsive-img");
        img_container.addHtmlElement(profile);

        wrapper.addHtmlElement(img_container);

        Div details = new Div();
        details.addCssClass("col s10");

        HtmlBasic.Span span = new HtmlBasic.Span();
        span.addCssClass("white-text");

        Div rw1 = new Div();
        rw1.addCssClass("row");
        rw1.addInnerContent("Created by " + HtmlBasic.generateBoldText("Bertalan Eck") + "<br/>" + "For job application");

        Div rw2 = new Div();
        rw2.addCssClass("row");
        rw2.addStyleElement("margin-bottom", "5px");
        HtmlBasic.MaterialIcon icon1 = new HtmlBasic.MaterialIcon("mail");
        icon1.addCssClass("left");
        icon1.addStyleElement("color", "white");
        rw2.addHtmlElement(icon1);
        rw2.addInnerContent("eckbertalan@hotmail.com");


        Div rw3 = new Div();
        rw3.addCssClass("row");
        rw3.addStyleElement("margin-bottom", "5px");
        HtmlBasic.MaterialIcon icon2 = new HtmlBasic.MaterialIcon("phone");
        icon2.addCssClass("left");
        icon2.addStyleElement("color", "white");
        rw3.addHtmlElement(icon2);
        rw3.addInnerContent("+36-70-431-57-19");

        Div rw4 = new Div();
        rw4.addCssClass("row");
        rw4.addStyleElement("margin-bottom", "5px");
        HtmlBasic.MaterialIcon icon3 = new HtmlBasic.MaterialIcon("location_on");
        icon3.addCssClass("left");
        icon3.addStyleElement("color", "white");
        rw4.addHtmlElement(icon3);
        rw4.addInnerContent("Hungary, 8412, Veszprém, Német u. 38");

        Div rw5 = new Div();
        rw5.addCssClass("row");
        rw5.addStyleElement("margin-bottom", "5px");
        HtmlBasic.MaterialIcon icon4 = new HtmlBasic.MaterialIcon("security");
        icon4.addCssClass("left");
        icon4.addStyleElement("color", "white");
        rw5.addHtmlElement(icon4);
        rw5.addInnerContent("Battle tag: Berci1992#2269");

        span.addHtmlElement(rw1);
        span.addHtmlElement(rw2);
        span.addHtmlElement(rw3);
        span.addHtmlElement(rw4);
        span.addHtmlElement(rw5);

        details.addHtmlElement(span);

        wrapper.addHtmlElement(details);

        card.addHtmlElement(wrapper);
    }

    /**
     * Adds the description part
     */
    private void addDescription(Div description)
    {
        description.addCssClass("red lighten-1 white-text");

            HtmlBasic.Paragraph p1 = new HtmlBasic.Paragraph("Using this little framework you are ablte to create your own HTML elements, and add it to a HTML page. Take a look at the class hierarchy. All you need to do is to derive subclasses from the abstract HtmlPrimitive or HtmlContainer class.");
            p1.addHtmlAttribute("align", "justify");
            p1.addStyleElement("margin-top", "0rem");
            p1.addStyleElement("padding", "0.5rem 0.5rem 0.2rem");

            HtmlBasic.Paragraph p2 = new HtmlBasic.Paragraph("You have to override the String getHtmlCode() method in order to make your own view element work. This method must return the appropriate html code for the element, including the opening and closing tags.");
            p2.addHtmlAttribute("align", "justify");
            p2.addStyleElement("margin-top", "0rem");
            p2.addStyleElement("padding", "0.5rem 0.5rem 0.2rem");

            HtmlBasic.Paragraph p3 = new HtmlBasic.Paragraph("For further information take a look at the documentation. I have used a third-party front-end framework for the design elements in this demo page.");
            p3.addHtmlAttribute("align", "justify");
            p3.addStyleElement("margin-top", "0rem");
            p3.addStyleElement("padding", "0.5rem 0.5rem 0.2rem");

            Div divider = new Div();
            divider.addStyleElement("width", "100%");
            divider.addStyleElement("border-bottom", "1px solid white");

            HtmlBasic.Paragraph p4 = new HtmlBasic.Paragraph("Examples");
            p4.addStyleElement("font-size", "2rem");
            p4.addStyleElement("padding", "0.5rem 0.5rem 0.2rem");
            p4.addCssClass("white-text");

        description.addHtmlElement(p1);
        description.addHtmlElement(p2);
        description.addHtmlElement(p3);
        description.addHtmlElement(divider);
        description.addHtmlElement(p4);
    }


    /**
     * Adds the examples
     */
    private void addExamples(HtmlList list)
    {
        list.addCssClass("collapsible white-text");
        list.addHtmlAttribute("data-collapsible", "accordion");
        list.addStyleElement("border-left", "0px");
        list.addStyleElement("border-right", "0px");

        //Li items
        //1
        Div col_header1 = new Div(new HtmlBasic.MaterialIcon("list").getHtmlCode() + "Select");
        col_header1.addCssClass("collapsible-header blue lighten-1");

        Select sct = new Select();
        sct.addOption("Mercedes","merci");
        sct.addOption("Audi","audi");
        sct.addOption("VW","vw");
        sct.addOption("Volvo","volvo");

        Div col_body1 = new Div(sct);
        col_body1.addCssClass("collapsible-body");

        String pre = HtmlBasic.generatePre("\t\t\t\t\tSelect sct = new Select();\n" +
                "\t\t\t\t\tsct.addOption(\"Mercedes\",\"merci\");\n" +
                "\t\t\t\t\tsct.addOption(\"Audi\",\"audi\");\n" +
                "\t\t\t\t\tsct.addOption(\"VW\",\"vw\");\n" +
                "\t\t\t\t\tsct.addOption(\"Volvo\",\"volvo\");");

        col_body1.addHtmlElement(new HtmlBasic.HtmlString(pre));

        HtmlList.Li l1 = new HtmlList.Li(col_header1);
        l1.addCssClass("red lighten-1");
        l1.addHtmlElement(col_body1);
        list.addHtmlElement(l1);

        //2
        Div col_header2 = new Div(new HtmlBasic.MaterialIcon("check_box").getHtmlCode() + "Checkbox");
        col_header2.addCssClass("collapsible-header blue lighten-1");

        Checkbox chb = new Checkbox("Checkbox", "chb1");
        chb.getLabel().addCssClass("white-text");
        HtmlBasic.Paragraph chp = new HtmlBasic.Paragraph();
        chp.addHtmlElement(chb);
        Div col_body2 = new Div(chp);
        col_body2.addCssClass("collapsible-body");

        pre = HtmlBasic.generatePre("\t\t\t\t\tCheckbox chb = new Checkbox(\"Checkbox\", \"chb\");\n" +
                "\t\t\t\t\tchb.getLabel().addCssClass(\"white-text\");");

        col_body2.addHtmlElement(new HtmlBasic.HtmlString(pre));
        HtmlList.Li l2 = new HtmlList.Li(col_header2);
        l2.addCssClass("red lighten-1");
        l2.addHtmlElement(col_body2);
        list.addHtmlElement(l2);

        //3
        Div col_header3 = new Div(new HtmlBasic.MaterialIcon("send").getHtmlCode() + "Button");
        col_header3.addCssClass("collapsible-header blue lighten-1");

        Button btn = new Button("Button example");
        btn.addCssClass("btn waves-effect waves-light");
        btn.addStyleElement("margin-left", "10px");
        btn.addStyleElement("margin-top", "10px");

        Div col_body3 = new Div(btn);
        col_body3.addCssClass("collapsible-body");

        pre = HtmlBasic.generatePre("\t\t\t\t\tButton btn = new Button(\"Button example\");\n" +
                "\t\t\t\t\tbtn.addCssClass(\"btn waves-effect waves-light\");\n" +
                "\t\t\t\t\tbtn.addStyleElement(\"margin-left\", \"10px\");\n" +
                "\t\t\t\t\tbtn.addStyleElement(\"margin-top\", \"10px\");");

        col_body3.addHtmlElement(new HtmlBasic.HtmlString(pre));
        HtmlList.Li l3 = new HtmlList.Li(col_header3);
        l3.addCssClass("red lighten-1");
        l3.addHtmlElement(col_body3);
        list.addHtmlElement(l3);
    }

    /**
     * Generates a web page form the demo code.
     */
    private void demo()
    {
        instance = HtmlFramework.getInstance();

        //add CSS and JS files
        Thread t1 = new Thread( () -> {
            addCSSFiles();
            addJSFiles();
        });
        t1.start();


        //add main container
        Div container = new Div();
        container.addCssClass("container z-depth-5");

        //add header
        Div header = new Div();
        Thread t2 = new Thread( () -> {
            addHeader(header);
        });
        t2.start();



        //add about the author
        Div card = new Div();
        Thread t3 = new Thread( () -> {
            addAboutAuthor(card);
        });
        t3.start();

        //add description
        Div description = new Div();
        Thread t4 = new Thread( () -> {
            addDescription(description);
        });
        t4.start();

        //add examples
        HtmlList list = new HtmlList(true);
        Thread t5 = new Thread( () -> {
            addExamples(list);
        });
        t5.start();

        //add elements to main container
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();

            container.addHtmlElement(header);
            container.addHtmlElement(card);
            container.addHtmlElement(description);
            description.addHtmlElement(list);

            instance.addHtmlElement(container);

            File file = instance.generateHTML("htmlframework", "Html Framework");
            if(Desktop.isDesktopSupported())
            {
                Desktop.getDesktop().browse(file.toURI());
                System.out.println("Your html file can be found in the html folder.");
            }
        } catch (InterruptedException e)
        {
            System.out.println("Something went wrong when synchronizing threads: ");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error writing to file: ");
            e.printStackTrace();
        }
    }

    /**
     * Entry point for the program. This program doesn't need any arguments
     */
    public static void main(String[] args)
    {
        new Main().demo();
    }

}
