package berci.htmlframework;

/**
 * The class simply represents a header HTML element
 */
public class Header extends HtmlPrimitive {

    private int level;
    /**
     * @param level Sets the header type, like h1, h2, h3, etc.
     * @param htmlValue Sets the text inside the header element
     */
    public Header(int level, String htmlValue) {
        super();
        if (level < 1) throw new IllegalArgumentException("Level can only be equal or higher than 1");
        this.level = level;
        innerHtmlValue = htmlValue;
    }

    @Override
    public String getHtmlCode()
    {

        StringBuilder builder = new StringBuilder();
        builder.append("<h");
        builder.append(level);
        builder.append(" ");
        builder.append(getAllHtmlFormatingElements());
        builder.append(">");
        builder.append(innerHtmlValue);
        builder.append("</h");
        builder.append(level);
        builder.append(">");

        return builder.toString();
    }
}
