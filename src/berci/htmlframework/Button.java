package berci.htmlframework;

/**
 * Class for Html Button tag.
 */

public class Button extends HtmlPrimitive{

     /**
     * @param value Sets the text on the Button
     */
    public Button(String value)
    {
        super();
        innerHtmlValue = value;
    }

    @Override
    public String getHtmlCode()
    {
        StringBuilder builder = new StringBuilder();

        builder.append("<button ");
        builder.append(getAllHtmlFormatingElements());
        builder.append(">");
        builder.append(innerHtmlValue);
        builder.append("</button>");


        return builder.toString();
    }
}
